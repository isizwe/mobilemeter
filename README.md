## Mobile Meter
<p>
This is a MobileMeter that displays different South African denominations of the inputted money after deducting a meter amount that can be edited on the frontend
</p>
<p>
<img height="550" src="mobileMeter001.gif"/>
</p>
