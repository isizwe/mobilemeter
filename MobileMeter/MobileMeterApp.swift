//
//  MobileMeterApp.swift
//  MobileMeter
//
//  Created by Isizwe Madalane on 2021/04/20.
//

import SwiftUI

@main
struct MobileMeterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
