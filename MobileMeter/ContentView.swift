//
//  ContentView.swift
//  MobileMeter
//
//  Created by Isizwe Madalane on 2021/04/20.
//

import SwiftUI

typealias Denomination = (key: String, value: Double)

struct ContentView: View {
    @State private var userAmount: String = ""
    @State private var meterAmount: String = "50" // Default Amount
    @State private var editMeterAmount: Bool = false
    private var denominationList: [Denomination] = [
        Denomination("10c", 0.1),
        Denomination("20c", 0.2),
        Denomination("50c", 0.5),
        Denomination("R1", 1),
        Denomination("R2", 2),
        Denomination("R5", 5),
        Denomination("R10", 10),
        Denomination("R20", 20),
        Denomination("R50", 50),
        Denomination("R100", 100),
        Denomination("R200", 200)
    ]
    
    var body: some View {
        let displayMeterAmount = Text("Meter Amount: \(meterAmount)")
        VStack {
            Group {
                HStack{
                    Toggle(isOn: $editMeterAmount, label: {
                        if editMeterAmount == true {
                            displayMeterAmount.hidden()
                            TextField("Meter Amount: default is 50", text: $meterAmount)
                        } else {
                            displayMeterAmount
                        }
                    })
                }.padding()
            }
            
            Group {
                Text("Total Amount")
                    .font(.largeTitle)
                    .fontWeight(.medium)
                Text("(After deductions)")
                    .font(.caption)
                Text("\(calculateTotal()?.amountInFormattedCurrency ?? "")")
                    .font(.largeTitle)
                    .fontWeight(.semibold)
                    .underline()
                TextField("Enter an Amount: e.g. 100", text: $userAmount)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .border(Color.black, width: 2)
                    .padding()
            }
            
            Group {
                Text("Denomination Breakdown:").underline()
                ForEach(denominationList, id: \.key) { (denomination) in
                    let displayAmount = denominationCalculation(denomination.value)
                    Text("\(denomination.key) \t\t\t x\(displayAmount)")
                }
            }.frame(maxWidth: .infinity)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension ContentView {
    private func denominationCalculation(_ moneyDenomination: Double) -> Int {
        guard let calculatedTotal = calculateTotal() else { return 0 }
        let result = calculatedTotal / moneyDenomination
        return Int(result)
    }
    
    private func calculateTotal() -> Double? {
        guard
            let inputUserAmount = Double(userAmount),
            let inputMeterAmount = Double(meterAmount)
        else { return 0.0 }
        return inputUserAmount - inputMeterAmount
    }
}

fileprivate extension Double {
    var amountInFormattedCurrency: String? {
        let formatter = formatToCurrency()
        let amount = NSNumber(value: self)
        let amountInLocalCurrency = formatter.string(from: amount)
        return amountInLocalCurrency
    }
    
    private func formatToCurrency() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.locale = .current
        formatter.currencyCode = "ZAR"
        formatter.currencySymbol = "R"
        formatter.internationalCurrencySymbol = "R"
        formatter.numberStyle = .currency
        return formatter
    }
}
